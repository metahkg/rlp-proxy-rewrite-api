import type { AxiosInstance, AxiosResponse, CancelToken } from "axios";
export declare class Client {
    private instance;
    private baseUrl;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined;
    constructor(baseUrl?: string, instance?: AxiosInstance);
    /**
     * Get metadata of a url
     * @param url The requested url (must be a valid https/http URL), UTF-8 encoded, see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent
     * @return Success
     */
    getMetadata(url: string, cancelToken?: CancelToken | undefined): Promise<Anonymous>;
    protected processGetMetadata(response: AxiosResponse): Promise<Anonymous>;
    /**
     * Alias of `getMetadata`
     * @param url The requested url (must be a valid https/http URL), UTF-8 encoded, see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent
     * @return Success
     */
    getMetadataV2(url: string, cancelToken?: CancelToken | undefined): Promise<Anonymous>;
    protected processGetMetadataV2(response: AxiosResponse): Promise<Anonymous>;
}
export interface Metadata {
    /** title of the site */
    title: string | null;
    /** description of the site */
    description: string | null;
    /** image url of the site */
    image: string | null;
    /** hmac signature of the image url, would be present if `cert/hmac.key` exists */
    image_signature?: string;
    /** site name of the site */
    siteName: string | null;
    /** hostname of the site (e.g. `metahkg.org`) */
    hostname: string | null;
}
export interface ErrorDto {
    /** http status code */
    statusCode: number;
    /** error message */
    error: string;
    /** detailed error message */
    message?: string;
}
export interface Anonymous {
    metadata: Metadata | null;
}
export declare class ApiException extends Error {
    message: string;
    status: number;
    response: string;
    headers: {
        [key: string]: any;
    };
    result: any;
    constructor(message: string, status: number, response: string, headers: {
        [key: string]: any;
    }, result: any);
    protected isApiException: boolean;
    static isApiException(obj: any): obj is ApiException;
}
